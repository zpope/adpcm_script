import os
import subprocess
import json

base_dir_originals = "C:/Wav Conversion/Original/"
string_program = "C:/Users/zpope/Documents/ima_adpcm/trunk/Release/VPencode.exe"
base_dir_output = "C:/Wav Conversion/Data output files/"

for file in os.listdir(base_dir_originals):
    if file.endswith(".wav"):
        out_file = file[:-3]
        out_file += "d"
        subprocess.Popen([string_program, base_dir_originals+file, base_dir_output+out_file],shell=True)

print("success")
